package com.example.pc.exposicionsharedpreferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    Button guardarPreferencias, mostrarPrefencias;
    EditText editTextUsuario, editTextCorreo;
    TextView textViewMostrar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        guardarPreferencias = findViewById(R.id.btnGuardarPreferencias);
        mostrarPrefencias = findViewById(R.id.btnMostrarPreferencias);
        editTextCorreo = findViewById(R.id.txtCorreo);
        editTextUsuario = findViewById(R.id.txtUsuario);
        textViewMostrar = findViewById(R.id.lblMostrar);
        guardarPreferencias.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                guardarPreferencia();
            }
        });
        mostrarPrefencias.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrarPreferencia();
            }
        });
    }
    public void guardarPreferencia(){
        SharedPreferences sharedPreferences = getSharedPreferences
                ("MisDatos", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor= sharedPreferences.edit();
        String Usuario = editTextUsuario.getText().toString();
        String Correo = editTextCorreo.getText().toString();

        editor.putString("Usuario", Usuario);
        editor.putString("Correo", Correo);
        editor.commit();
        Toast.makeText(MainActivity.this,
                "Se creo correctamente la preferencia", Toast.LENGTH_SHORT).show();
        editTextUsuario.setText("");
        editTextCorreo.setText("");

    }
    public void mostrarPreferencia(){
        SharedPreferences sharedPreferences =
                getSharedPreferences("MisDatos", Context.MODE_PRIVATE);
        String Usuario = sharedPreferences.getString("Usuario","");
        String Correo = sharedPreferences.getString("Correo", "");
        String preferencia = "\nUsuario:" + Usuario + "\nCorreo:" + Correo;
        textViewMostrar.setText(preferencia);
    }

}
